import axiosClient from "./axiosClient";

const postApi = {
  getPosts: () => {
    const url = "posts";
    return axiosClient.get(url);
  },
  addNewPost: (params) => {
    const url = "posts";
    return axiosClient.post(url, params);
  },
  delete: (postId) => {
    const url = `posts/${postId}`;
    return axiosClient.delete(url);
  },
  update: (postId, updatedPost) => {
    const url = `posts/${postId}`;
    return axiosClient.put(url, updatedPost);
  },
};

export default postApi;
