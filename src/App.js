import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from "./components/layout/Landing";
import Auth from "./views/Auth";
import Consumer from "./views/consumer/Consumer";
import ProtectedRoute from "./routing/ProtectedRoute";
import Admin from "./views/admin/Admin";
import { useContext } from "react";
import { AuthContext } from "./contexts/AuthContext";

function App() {
  const loginUser = useContext(AuthContext);
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Landing} loginUser={loginUser} />
        <Route
          exact
          path="/login"
          render={(props) => <Auth {...props} authRoute="login" />}
        />
        <Route
          exact
          path="/register"
          render={(props) => <Auth {...props} authRoute="register" />}
        />
        <ProtectedRoute
          exact
          path="/admin"
          component={Admin}
          isAdmin={true}
          loginUser={loginUser}
        />
        <ProtectedRoute
          exact
          path="/consumer"
          component={Consumer}
          isAdmin={false}
          loginUser={loginUser}
        />
      </Switch>
    </Router>
  );
}

export default App;
