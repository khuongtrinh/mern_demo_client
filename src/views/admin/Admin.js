import React, { useContext, useEffect } from "react";
import { Button, Col, Row, Spinner } from "react-bootstrap";
import NavbarMenu from "../../components/layout/NavbarMenu";
import AddPostModal from "../../components/posts/AddPostModal";
import SinglePost from "../../components/posts/SinglePost";
import UpdatePostModal from "../../components/posts/UpdatePostModal";
import { PostContext } from "../../contexts/PostContext";

const Admin = () => {
  const {
    postState: { post, posts, postsLoading },
    getAllPost,
    setShowAddPostModal,
  } = useContext(PostContext);

  useEffect(() => {
    getAllPost();
  }, []);

  let body = null;
  if (postsLoading) {
    body = (
      <div className="spinner-container">
        <Spinner animation="border" variant="info" />
      </div>
    );
  } else {
    body = (
      <>
        <Row className="row-cols-1 row-cols-md-3 g-4 mx-auto mt-3">
          {posts.map((post) => (
            <Col key={post._id} className="my-2">
              <SinglePost post={post} />
            </Col>
          ))}
        </Row>
        {/* Open Add Post Modal */}
        <Button
          className="btn-floating"
          onClick={setShowAddPostModal.bind(this, true)}
        >
          +
        </Button>
      </>
    );
  }
  return (
    <>
      <NavbarMenu />
      <div>{body}</div>
      <AddPostModal />
      {post && <UpdatePostModal />}
    </>
  );
};

export default Admin;
