import React, { useContext } from "react";
import RegisterForm from "../components/auth/RegisterForm";
import LoginForm from "../components/auth/LoginForm";
import { AuthContext } from "../contexts/AuthContext";
import { Redirect } from "react-router-dom";
import { Spinner } from "react-bootstrap";

const Auth = ({ authRoute }) => {
  const {
    authState: { authLoading, isAuthenticated, user },
  } = useContext(AuthContext);
  let body;
  if (authLoading) {
    <div className="d-flex justify-content-center mt-2">
      <Spinner animation="border" variant="info" />
    </div>;
  } else if (isAuthenticated) {
    if (user.isAdmin) {
      return <Redirect to="/admin" />;
    }
    return <Redirect to="/consumer" />;
  }
  body = (
    <>
      {authRoute === "login" && <LoginForm />}
      {authRoute === "register" && <RegisterForm />}
    </>
  );
  return (
    <div className="landing">
      <div className="dark-overlay">
        <div className="landing-inner">
          <h1>Welcome to Anime World</h1>
          <h4>Are you looking for something new?</h4>
          {body}
        </div>
      </div>
    </div>
  );
};

export default Auth;
