import React, { useContext, useEffect } from "react";
import { Button, Card, Col, Row, Spinner } from "react-bootstrap";
import NavbarMenu from "../../components/layout/NavbarMenu";
import { PostContext } from "../../contexts/PostContext";

const Consumer = () => {
  const {
    postState: { posts, postsLoading },
    getAllPost,
  } = useContext(PostContext);

  useEffect(() => {
    getAllPost();
  }, []);

  let body = null;
  if (postsLoading) {
    body = (
      <div className="spinner-container">
        <Spinner animation="border" variant="info" />
      </div>
    );
  } else {
    body = (
      <>
        <Row className="row-cols-1 row-cols-md-3 g-4 mx-auto mt-3">
          {posts.map((post) => (
            <Col key={post._id} className="my-2">
              <Card>
                <Card.Img variant="top" src={post.image} />
                <Card.Body>
                  <Card.Title>{post.title}</Card.Title>
                  <Card.Text>{post.description}</Card.Text>
                  <Button
                    variant="primary"
                    onClick={() => (window.location = post.url)}
                  >
                    Link
                  </Button>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </>
    );
  }

  return (
    <>
      <NavbarMenu />
      <div>{body}</div>
    </>
  );
};

export default Consumer;
