import React from "react";
import { Route, Redirect } from "react-router-dom";
import { Spinner } from "react-bootstrap";
const ProtectedRoute = ({
  component: Component,
  isAdmin,
  loginUser,
  ...rest
}) => {
  const {
    authState: { authLoading, isAuthenticated, user },
  } = loginUser;
  if (authLoading) {
    return (
      <div className="spinner-container">
        <Spinner animation="border" variant="info" />
      </div>
    );
  }
  return (
    <Route
      {...rest}
      render={(props) => {
        if (isAuthenticated) {
          if (isAdmin === user.isAdmin) {
            return <Component {...rest} {...props} />;
          } else {
            return <Redirect to="/" />;
          }
        } else {
          return <Redirect to="/login" />;
        }
      }}
    />
  );
};

export default ProtectedRoute;
