import { createContext, useEffect, useReducer } from "react";
import { LOCAL_STORAGE_TOKEN_NAME } from "./constants";
import { authReducer } from "../reducers/authReducer";
import setAuthToken from "../utils/setAuthToken";
import authApi from "../api/authApi";

export const AuthContext = createContext();

const AuthContextProvider = ({ children }) => {
  const [authState, dispatch] = useReducer(authReducer, {
    authLoading: true,
    isAuthenticated: false,
    user: null,
  });
  //Authenticate user
  const loadUser = async () => {
    if (localStorage[LOCAL_STORAGE_TOKEN_NAME]) {
      setAuthToken(localStorage[LOCAL_STORAGE_TOKEN_NAME]);
    }
    try {
      const response = await authApi.getUser();
      if (response.success) {
        dispatch({
          type: "SET_AUTH",
          payload: { isAuthenticated: true, user: response.user },
        });
      }
    } catch (error) {
      localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME);
      setAuthToken(null);
      dispatch({
        type: "SET_AUTH",
        payload: { isAuthenticated: false, user: null },
      });
    }
  };
  useEffect(() => {
    loadUser();
  }, []);
  //Login
  const LoginUser = async (userForm) => {
    try {
      const response = await authApi.login(userForm);
      if (response.success) {
        localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, response.accessToken);
      }
      await loadUser();
      return response;
    } catch (error) {
      if (error.response) {
        return error.response;
      } else {
        return { success: false, message: error.message };
      }
    }
  };
  //Register
  const RegisterUser = async (userForm) => {
    try {
      const response = await authApi.regist(userForm);
      if (response.success) {
        localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, response.accessToken);
      }
      await loadUser();
      return response;
    } catch (error) {
      if (error.response) {
        return error.response;
      } else {
        return { success: false, message: error.message };
      }
    }
  };
  // Logout
  const LogoutUser = () => {
    localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME);
    dispatch({
      type: "SET_AUTH",
      payload: { isAuthenticated: false, user: null },
    });
  };
  //Context data
  const authContextData = { LoginUser, RegisterUser, LogoutUser, authState };
  //Return provider
  return (
    <AuthContext.Provider value={authContextData}>
      {children}
    </AuthContext.Provider>
  );
};
export default AuthContextProvider;
