import { createContext, useReducer, useState } from "react";
import { postReducer } from "../reducers/postReducer";
import postApi from "../api/postApi";
import {
  ADD_POST,
  POSTS_LOADED_FAIL,
  POSTS_LOADED_SUCCESS,
  DELETE_POST,
  UPDATE_POST,
  FIND_POST,
} from "./constants";

export const PostContext = createContext();

const PostContextProvider = ({ children }) => {
  const [postState, dispatch] = useReducer(postReducer, {
    posts: [],
    postsLoading: true,
    post: null,
  });

  const [showAddPostModal, setShowAddPostModal] = useState(false);
  const [showUpdatePostModal, setShowUpdatePostModal] = useState(false);

  //get posts
  const getAllPost = async () => {
    try {
      const response = await postApi.getPosts();
      if (response.success) {
        dispatch({ type: POSTS_LOADED_SUCCESS, payload: response.posts });
      }
    } catch (error) {
      dispatch({ type: POSTS_LOADED_FAIL });
    }
  };

  // Add post
  const addPost = async (newPost) => {
    try {
      const response = await postApi.addNewPost(newPost);
      if (response.success) {
        dispatch({ type: ADD_POST, payload: response.post });
        return response;
      }
    } catch (error) {
      return error.response
        ? error.response
        : { success: false, message: "Server error" };
    }
  };

  // Delete post
  const deletePost = async (postId) => {
    try {
      const response = await postApi.delete(postId);
      if (response.success) dispatch({ type: DELETE_POST, payload: postId });
    } catch (error) {
      console.log(error);
    }
  };

  // Find post when user is updating post
  const findPost = (postId) => {
    const post = postState.posts.find((post) => post._id === postId);
    dispatch({ type: FIND_POST, payload: post });
  };

  // Update post
  const updatePost = async (updatedPost) => {
    try {
      const response = await postApi.update(updatedPost._id, updatedPost);
      if (response.success) {
        dispatch({ type: UPDATE_POST, payload: response.post });
        return response;
      }
    } catch (error) {
      return error.response
        ? error.response
        : { success: false, message: "Server error" };
    }
  };

  //post Context Data

  const postContextData = {
    postState,
    getAllPost,
    addPost,
    deletePost,
    updatePost,
    findPost,
    showAddPostModal,
    setShowAddPostModal,
    showUpdatePostModal,
    setShowUpdatePostModal,
  };

  return (
    <PostContext.Provider value={postContextData}>
      {children}
    </PostContext.Provider>
  );
};

export default PostContextProvider;
