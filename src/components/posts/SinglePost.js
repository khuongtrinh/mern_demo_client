import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Badge from "react-bootstrap/Badge";
import ActionButtons from "./ActionButtons";
import { Button } from "react-bootstrap";

const SinglePost = ({
  post: { _id, status, title, description, url, image },
}) => (
  <Card className="shadow" border={status === "active" ? "success" : "danger"}>
    <Card.Img variant="top" src={image} />
    <Card.Body>
      <Card.Title>
        <Row>
          <Col>
            <p className="post-title">{title}</p>
            <Badge pill variant={status === "active" ? "success" : "danger"}>
              {status}
            </Badge>
          </Col>
          <Col className="text-right">
            <ActionButtons url={url} _id={_id} />
          </Col>
        </Row>
      </Card.Title>
      <Card.Text>{description}</Card.Text>
      <Button variant="primary" onClick={() => (window.location = url)}>
        Link
      </Button>
    </Card.Body>
  </Card>
);

export default SinglePost;
