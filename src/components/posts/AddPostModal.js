import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useContext, useState } from "react";
import { PostContext } from "../../contexts/PostContext";
import FileBase64 from "react-file-base64";

const AddPostModal = () => {
  // Contexts
  const { addPost, getAllPost, showAddPostModal, setShowAddPostModal } =
    useContext(PostContext);

  // State
  const [newPost, setNewPost] = useState({
    image: "",
    title: "",
    description: "",
    url: "",
    status: "active",
  });

  const { title, description, url, status } = newPost;

  const onChangeNewPostForm = (event) =>
    setNewPost({ ...newPost, [event.target.name]: event.target.value });

  const closeDialog = () => {
    resetAddPostData();
  };

  const onSubmit = async (event) => {
    event.preventDefault();
    await addPost(newPost);
    await getAllPost();
    resetAddPostData();
  };

  const resetAddPostData = () => {
    setNewPost({
      image: null,
      title: "",
      description: "",
      url: "",
      status: "active",
    });
    setShowAddPostModal(false);
  };

  return (
    <Modal show={showAddPostModal} onHide={closeDialog} animation={false}>
      <Modal.Header closeButton>
        <Modal.Title>Add new anime character</Modal.Title>
      </Modal.Header>
      <Form onSubmit={onSubmit}>
        <Modal.Body>
          <Form.Group>
            <FileBase64
              accept="/image*"
              multiple={false}
              type="file"
              onDone={({ base64 }) => setNewPost({ ...newPost, image: base64 })}
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Title"
              name="title"
              required
              aria-describedby="title-help"
              value={title}
              onChange={onChangeNewPostForm}
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="Description"
              name="description"
              value={description}
              onChange={onChangeNewPostForm}
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              type="text"
              placeholder="Wikipedia"
              name="url"
              value={url}
              onChange={onChangeNewPostForm}
            />
          </Form.Group>
          <Form.Group>
            <Form.Control
              as="select"
              value={status}
              name="status"
              onChange={onChangeNewPostForm}
            >
              <option value="active">active</option>
              <option value="inactive">inactive</option>
            </Form.Control>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeDialog}>
            Cancel
          </Button>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default AddPostModal;
