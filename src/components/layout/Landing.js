import React, { useContext } from "react";
import { Redirect } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";

const Landing = () => {
  const loginUser = useContext(AuthContext);
  const {
    authState: { user },
  } = loginUser;
  let redirectPath;
  if (user) {
    if (user.isAdmin) {
      redirectPath = "/admin";
    } else {
      redirectPath = "/dashboard";
    }
  } else {
    redirectPath = "/login";
  }
  return <Redirect to={redirectPath} />;
};

export default Landing;
